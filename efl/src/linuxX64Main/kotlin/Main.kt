package com.serebit.wraith.efl

import efl.*
import kotlinx.cinterop.*
import kotlin.system.exitProcess

fun main(args: Array<String>) = memScoped<Unit> {
    val real: Int
    _efl_startup_time = ecore_time_unix_get()
    efl_app_version_set()
    ecore_init()
    efl_event_callback_priority_add(
        efl_main_loop_get(),
        EFL_LOOP_EVENT_ARGUMENTS,
        EFL_CALLBACK_PRIORITY_DEFAULT.toShort(),
        staticCFunction(::eflMain),
        null
    )
    efl_main_constructor(args.size, args.takeIf { it.isNotEmpty() }?.toCStringArray(memScope))
    val ret: CPointer<Eina_Value>? = efl_loop_begin(efl_main_loop_get())
    real = efl_loop_exit_code_process(ret)
    efl_main_destructor()
    ecore_shutdown_ex()
    ecore_shutdown()
    exitProcess(real)
}

@Suppress("unused_parameter")
fun eflMain(data: COpaquePointer?, ev: CPointer<Efl_Event>?) {
    guiSetup()
}

fun guiSetup() {
    val win: CPointer<Eo> = eflAddKt(EFL_UI_WIN_CLASS, efl_main_loop_get()) {
        efl_text_set(it, "Wraith Master")
        efl_event_callback_priority_add(
            it,
            EFL_UI_WIN_EVENT_DELETE_REQUEST,
            EFL_CALLBACK_PRIORITY_DEFAULT.toShort(),
            staticCFunction(::guiQuitCallback),
            null
        )
    }

    val box = eflAddKt(EFL_UI_BOX_CLASS, win) {
        efl_content_set(win, it)
        efl_gfx_hint_size_min_set(it, eina_size2d(450, 400))
        efl_gfx_entity_scale_set(it, 1.2)
    }

    val tabPager = eflAddKt(EFL_UI_TAB_PAGER_CLASS, box) {
        efl_gfx_hint_weight_set(it, 1.0, 0.8)
        efl_pack_end(box, it)
    }

    eflAddKt(EFL_UI_TAB_PAGE_CLASS, tabPager) {
        val tabBarItem = efl_ui_tab_page_tab_bar_item_get(it)
        efl_text_set(tabBarItem, "Ring")
        efl_pack_end(tabPager, it)
    }

    fun setupOptionBox(tabPageBox: CValuesRef<Eo>?, label: String) {
        eflAddKt(EFL_UI_BOX_CLASS, tabPageBox) { optionBox ->
            efl_ui_layout_orientation_set(optionBox, EFL_UI_LAYOUT_ORIENTATION_HORIZONTAL)
            efl_gfx_hint_margin_set(optionBox, 0, 0, 0, 8)
            eflAddKt(EFL_UI_TEXTBOX_CLASS, optionBox) { labelTextBox ->
                efl_text_set(labelTextBox, label)
                efl_text_interactive_editable_set(labelTextBox, EINA_FALSE)
                efl_text_interactive_selection_allowed_set(labelTextBox, EINA_FALSE)
                efl_text_font_size_set(labelTextBox, 12)
                efl_text_color_set(labelTextBox, 255, 255, 255, 255)
                efl_text_vertical_align_set(labelTextBox, 0.5)
                efl_pack(optionBox, labelTextBox)
            }
            eflAddKt(EFL_UI_SPIN_BUTTON_CLASS, optionBox) { placeholderSpinner ->
                efl_ui_range_limits_set(placeholderSpinner, 1.0, 5.0)
                efl_pack(optionBox, placeholderSpinner)
            }
            efl_pack(tabPageBox, optionBox)
        }
    }

    eflAddKt(EFL_UI_TAB_PAGE_CLASS, tabPager) { fanPage ->
        val tabBarItem = efl_ui_tab_page_tab_bar_item_get(fanPage)
        efl_text_set(tabBarItem, "Fan")
        eflAddKt(EFL_UI_BOX_CLASS, fanPage) { fanPageBox ->
            efl_gfx_hint_margin_set(fanPageBox, 16, 16, 16, 16)
            setupOptionBox(fanPageBox, "Mode")
            setupOptionBox(fanPageBox, "Color")
            setupOptionBox(fanPageBox, "Brightness")
            setupOptionBox(fanPageBox, "Speed")
            setupOptionBox(fanPageBox, "Rotation Direction")
            setupOptionBox(fanPageBox, "Morse Text")
            efl_content_set(fanPage, fanPageBox)
        }
        efl_pack_end(tabPager, fanPage)
    }

    eflAddKt(EFL_UI_TAB_PAGE_CLASS, tabPager) {
        val tabBarItem = efl_ui_tab_page_tab_bar_item_get(it)
        efl_text_set(tabBarItem, "Logo")
        efl_pack_end(tabPager, it)
    }

    val buttonBox = eflAddKt(EFL_UI_BOX_FLOW_CLASS, box) {
        efl_gfx_hint_weight_set(it, 1.0, 0.1)
        efl_ui_layout_orientation_set(it, 1u)
        efl_gfx_hint_margin_set(it, 5, 5, 2, 2)
        efl_gfx_entity_scale_set(it, 1.2)
        efl_ui_box_homogeneous_set(it, EINA_FALSE)
        efl_pack_end(box, it)
    }

    eflAddKt(EFL_UI_BUTTON_CLASS, buttonBox) {
        efl_text_set(it, "Reset")
        efl_ui_widget_disabled_set(it, EINA_TRUE)
        efl_gfx_hint_weight_set(it, 0.0, 0.0)
        efl_gfx_hint_align_set(it, 1.0, 0.5)
        efl_gfx_hint_fill_set(it, EINA_FALSE, EINA_FALSE)
        efl_pack_end(buttonBox, it)
    }

    eflAddKt(EFL_UI_BUTTON_CLASS, buttonBox) {
        efl_text_set(it, "Apply")
        efl_gfx_hint_weight_set(it, 0.0, 0.0)
        efl_gfx_hint_align_set(it, 1.0, 0.5)
        efl_gfx_hint_fill_set(it, EINA_FALSE, EINA_FALSE)
        efl_pack_end(buttonBox, it)
    }
}

@Suppress("unused_parameter")
fun guiQuitCallback(data: COpaquePointer?, ev: CPointer<Efl_Event>?) {
    efl_exit(0)
}

private fun eflAddKt(
    klass: CPointer<Efl_Class>?,
    parent: CValuesRef<Eo>?,
    init: (CValuesRef<Eo>) -> Unit = {}
): CPointer<Eo> {
    val added = _efl_add_internal_start("NO FILE", -1, klass, parent, EINA_FALSE, EINA_TRUE)
        ?: error("Failed to initialize widget using _efl_add_internal_start")

    init(added)

    return _efl_add_end(added, EINA_FALSE, EINA_TRUE)
        ?: error("Failed to finalize widget using _efl_add_end")
}
