package com.serebit.wraith.core

import cnames.structs.libusb_device_handle
import kotlinx.cinterop.*
import libusb.*
import platform.posix.getenv

class TransferError(code: Int) : Throwable(libusb_strerror(code)!!.toKString())

internal class UsbInterface(private val requestSize: Int, private val handle: CPointer<libusb_device_handle>) {
    private val printTransfers = getenv("WRAITH_MASTER_DEBUG")?.toKString() == "1"

    fun sendBytes(vararg bytes: UByte, filler: UByte = 0u): List<UByte> = memScoped {
        val outBytes = UByteArray(requestSize) { (bytes.getOrNull(it) ?: filler).toUByte() }
        transfer(ENDPOINT_OUT, outBytes)
        transfer(ENDPOINT_IN, UByteArray(requestSize)).toList()
    }

    fun resetPort() {
        libusb_reset_device(handle)
    }

    fun close() {
        libusb_release_interface(handle, HID_INTERFACE)
        libusb_close(handle)
    }

    private fun transfer(endpoint: UByte, bytes: UByteArray): UByteArray = memScoped {
        val bytesPtr = bytes.toCValues().ptr
        val err = libusb_interrupt_transfer(handle, endpoint, bytesPtr, requestSize, null, TIMEOUT)
        if (err != LIBUSB_SUCCESS) {
            throw TransferError(err)
        }
        val returned = bytesPtr.pointed.readValues(requestSize).getBytes().toUByteArray()

        if (printTransfers) {
            val endpointStr = if (endpoint == ENDPOINT_IN) "IN " else "OUT"
            println("Sent ${bytes.size} bytes to endpoint $endpointStr:       ${bytes.joinToDebugString()}")
            println("Received ${bytes.size} bytes from endpoint $endpointStr: ${returned.joinToDebugString()}")
            println()
        }

        returned
    }

    private fun UByteArray.joinToDebugString(): String =
        map { it.toString(16).uppercase().padStart(2, '0') }.joinToString(" ", "[", "]")

    companion object {
        private const val ENDPOINT_IN: UByte = 0x83u
        private const val ENDPOINT_OUT: UByte = 4u
        private const val HID_INTERFACE: Int = 1
        private const val TIMEOUT: UInt = 1000u
    }
}
