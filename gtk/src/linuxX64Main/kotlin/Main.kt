package com.serebit.wraith.gtk

import com.serebit.wraith.core.DeviceResult
import com.serebit.wraith.core.TransferError
import com.serebit.wraith.core.obtainDevices
import com.serebit.wraith.core.prism.WraithPrism
import com.serebit.wraith.core.prism.hasUnsavedChanges
import com.serebit.wraith.core.prism.resetToDefault
import gtk3.*
import kotlinx.cinterop.*
import libusb.libusb_exit
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    val app = gtk_application_new("com.serebit.wraith", G_APPLICATION_FLAGS_NONE)!!

    val results = obtainDevices()
    val successes = results.filterIsInstance<DeviceResult.Success>()
    val failures = results.filterIsInstance<DeviceResult.Failure>()

    when {
        results.isEmpty() -> app.connectToSignal("activate") {
            val dialog = gtk_message_dialog_new(
                null, 0u, GtkMessageType.GTK_MESSAGE_ERROR, GtkButtonsType.GTK_BUTTONS_OK,
                "%s", "Couldn't find any compatible devices. Make sure the internal USB 2.0 cable is connected."
            )!!

            gtk_dialog_run(dialog.reinterpret())

            gtk_widget_destroy(dialog)
        }
        failures.isNotEmpty() -> app.connectToSignal("activate") {
            val dialog = gtk_message_dialog_new(
                null,
                0u,
                GtkMessageType.GTK_MESSAGE_ERROR,
                GtkButtonsType.GTK_BUTTONS_OK,
                "%s",
                "Found ${failures.size} devices that produced errors when attempting to connect:\n\n${
                    failures.joinToString("\n\n")
                }"
            )!!

            gtk_dialog_run(dialog.reinterpret())

            gtk_widget_destroy(dialog)
        }
        successes.isNotEmpty() -> app.connectToSignal("activate") {
            app.createWindowOrNull { activate(successes.map { it.prism }) } ?: runNoExtraWindowsDialog()
        }
    }

    val status: Int = try {
        memScoped { g_application_run(app.reinterpret(), args.size, args.map { it.cstr.ptr }.toCValues()) }
    } catch (err: TransferError) {
        err.printStackTrace()
        1
    }

    g_object_unref(app)

    successes.forEach { it.prism.close() }
    libusb_exit(null)

    if (status != 0) exitProcess(status)
}

fun CPointer<GtkApplication>.createWindowOrNull(addWidgets: Widget.() -> Unit): Widget? =
    if (gtk_application_get_active_window(this) == null) gtk_application_window_new(this)!!.apply {
        clearFocusOnClickOrEsc()

        val headerBar = gtk_header_bar_new()!!.apply {
            gtk_header_bar_set_show_close_button(reinterpret(), 1)
            gtk_header_bar_set_title(reinterpret(), "Wraith Master")
            val aboutButton = iconButton("dialog-information", null).apply {
                connectToSignal("clicked") {
                    runAboutDialog()
                }
            }
            gtk_header_bar_pack_start(reinterpret(), aboutButton)
        }

        gtk_window_set_titlebar(reinterpret(), headerBar)
        gtk_window_set_icon_name(reinterpret(), "wraith-master")
        gtk_window_set_default_icon_name("applications-games")
        addWidgets()

        gtk_window_set_position(reinterpret(), GtkWindowPosition.GTK_WIN_POS_CENTER)
        gtk_widget_show_all(this)
    } else null

fun Widget.activate(devices: List<WraithPrism>) {
    val mainBox = gtk_box_new(GtkOrientation.GTK_ORIENTATION_HORIZONTAL, 0)!!.also {
        gtk_container_add(reinterpret(), it)
    }

    val stackSidebar = gtk_stack_sidebar_new()!!.also {
        gtk_container_add(mainBox.reinterpret(), it)
    }

    gtk_container_add(mainBox.reinterpret(), gtk_separator_new(GtkOrientation.GTK_ORIENTATION_VERTICAL))

    val stack = gtk_stack_new()!!.also {
        gtk_stack_sidebar_set_stack(stackSidebar.reinterpret(), it.reinterpret())
        gtk_stack_set_transition_type(it.reinterpret(), GtkStackTransitionType.GTK_STACK_TRANSITION_TYPE_SLIDE_UP_DOWN)
        gtk_container_add(mainBox.reinterpret(), it)
    }

    data class MainCallbackData(
        val wraith: WraithPrism,
        val widgets: List<PrismComponentWidgets>,
        val buttons: List<Widget>
    )

    val callbackDataList = devices.map { device ->
        val box = gtk_box_new(GtkOrientation.GTK_ORIENTATION_VERTICAL, 0)!!.also {
            gtk_stack_add_titled(stack.reinterpret(), it, "Wraith Prism", "Wraith Prism")
        }

        val mainNotebook = gtk_notebook_new()!!.apply {
            gtk_container_add(box.reinterpret(), this)
            addCss("notebook header.top tabs tab { padding-left: 20px; padding-right: 20px; margin-right: 4px; }")
        }

        val ringGrid = mainNotebook.newSettingsPage("Ring").newSettingsGrid()
        val fanGrid = mainNotebook.newSettingsPage("Fan").newSettingsGrid()
        val logoGrid = mainNotebook.newSettingsPage("Logo").newSettingsGrid()

        val ringWidgets = RingWidgets(device).apply { initialize(ringGrid) }
        val fanWidgets = FanWidgets(device).apply { initialize(fanGrid) }
        val logoWidgets = LogoWidgets(device).apply { initialize(logoGrid) }

        val saveOptionBox = gtk_button_box_new(GtkOrientation.GTK_ORIENTATION_HORIZONTAL)!!.apply {
            gtk_container_add(box.reinterpret(), this)
            gtk_container_set_border_width(reinterpret(), 10u)
            gtk_button_box_set_layout(reinterpret(), GTK_BUTTONBOX_END)
            gtk_box_set_spacing(reinterpret(), 8)
            gtk_box_set_child_packing(box.reinterpret(), this, 0, 1, 0u, GtkPackType.GTK_PACK_END)
        }

        val resetButton = gtk_button_new()!!.apply {
            gtk_button_set_label(reinterpret(), "Reset")
            setSensitive(false)
            gtk_container_add(saveOptionBox.reinterpret(), this)
        }

        val saveButton = gtk_button_new()!!.apply {
            gtk_button_set_label(reinterpret(), "Save")
            gtk_style_context_add_class(gtk_widget_get_style_context(this), "suggested-action")
            setSensitive(false)
            gtk_container_add(saveOptionBox.reinterpret(), this)
        }

        val saveOptionButtons = listOf(resetButton, saveButton)
        val allWidgets = listOf(ringWidgets, fanWidgets, logoWidgets)
        val callbackData = MainCallbackData(device, allWidgets, saveOptionButtons)

        gtk_window_get_titlebar(reinterpret())!!.apply {
            val firmwareVersion = device.requestFirmwareVersion()
            gtk_header_bar_set_subtitle(reinterpret(), "Wraith Prism, firmware $firmwareVersion")

            val resetToDefaultButton = gtk_menu_item_new_with_label("Reset to Default")!!.apply {
                connectToSignal("activate") {
                    device.resetToDefault()

                    allWidgets.filterIsInstance<FanWidgets>().forEach { it.setMirageEnabled(330, 330, 330) }
                    allWidgets.forEach { it.reload() }
                }
                gtk_widget_show(this)
            }

            val toggleEnsoModeButton = gtk_menu_item_new_with_label("Toggle Enso Mode")!!.apply {
                connectToSignal("activate") {
                    device.apply {
                        enso = !enso
                        if (!enso) {
                            device.resetToDefault()
                            save()
                        }
                    }
                    allWidgets.forEach { it.reload() }
                }
                gtk_widget_show(this)
            }

            val resetPortButton = gtk_menu_item_new_with_label("Reset USB Port")!!.apply {
                connectToSignal("activate") {
                    device.resetPort()
                }
                gtk_widget_show(this)
            }

            val dropdownMenuButton = gtk_menu_button_new()!!.apply {
                gtk_menu_button_set_popup(reinterpret(), gtk_menu_new()!!.apply {
                    gtk_menu_attach(reinterpret(), resetToDefaultButton, 0, 1, 0, 1)
                    gtk_menu_attach(reinterpret(), toggleEnsoModeButton, 0, 1, 1, 2)
                    gtk_menu_attach(reinterpret(), resetPortButton, 0, 1, 2, 3)
                })
            }

            gtk_header_bar_pack_start(reinterpret(), dropdownMenuButton)
        }

        resetButton.connectToSignal("clicked") {
            device.restore()
            device.apply()
            device.components.forEach { it.reloadValues() }
            allWidgets.forEach { it.reload() }
            saveOptionButtons.forEach { it.setSensitive(false) }
        }

        saveButton.connectToSignal("clicked") {
            device.save()
            saveOptionButtons.forEach { it.setSensitive(false) }
        }

        device.onApply = {
            saveOptionButtons.forEach { it.setSensitive(device.hasUnsavedChanges) }
        }

        callbackData
    }

    connectToSignal("delete-event", StableRef.create(callbackDataList).asCPointer(),
        staticCFunction<Widget, CPointer<GdkEvent>, COpaquePointer, Boolean> { window, _, ptr ->
            var returnValue = false
            val callbackData = ptr.asStableRef<List<MainCallbackData>>().get()

            val hasUnsavedChanges = callbackData.map { it.wraith }.filter { it.hasUnsavedChanges }

            if (hasUnsavedChanges.isNotEmpty()) {
                val dialog = gtk_message_dialog_new(
                    window.reinterpret(), 0u, GtkMessageType.GTK_MESSAGE_QUESTION, GtkButtonsType.GTK_BUTTONS_NONE,
                    "%s", "You have unsaved changes. Would you like to save them?"
                )!!
                gtk_dialog_add_buttons(
                    dialog.reinterpret(),
                    "Yes", GTK_RESPONSE_YES,
                    "No", GTK_RESPONSE_NO,
                    "Cancel", GTK_RESPONSE_CANCEL,
                    null
                )

                when (gtk_dialog_run(dialog.reinterpret())) {
                    GTK_RESPONSE_YES -> hasUnsavedChanges.forEach { it.save() }
                    GTK_RESPONSE_NO -> hasUnsavedChanges.forEach {
                        it.restore()
                        it.apply(runCallback = false)
                    }
                    GTK_RESPONSE_CANCEL -> returnValue = true
                }

                gtk_widget_destroy(dialog)
            }

            if (!returnValue) {
                callbackData.forEach { datum ->
                    datum.widgets.forEach { it.close() }
                }
                ptr.asStableRef<List<MainCallbackData>>().dispose()
            }

            returnValue
        })
}
